Navigation:

Step0. run roscore

Step1. "roslaunch dashgo_bringup bringup.launch" on D1.

Step2. "roslaunch dashgo_nav navigation.launch" and you 
       can see the detail in Rviz.

Step3. "roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch" and drive
       D1 to the right position.

Step4. Now you can let D1 do navagition itself.

====================

Slam Map:

Step0. run roscore

Step1. "roslaunch dashgo_nav gmapping_demo.launch" on D1.

Step2. "roslaunch turtlebot_rviz_launchers view_navigation.launch"

Step3. "roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch"

Step4. When finish slam the map. "rosrun map_server map_saver -f my_map"
